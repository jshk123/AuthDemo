package com.noyes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * @author yan cong
 * @className SpringbootStart
 * @description
 * @time 2021/6/3 13:48
 */
@SpringBootApplication
@EnableWebSecurity
public class SpringbootSecurityStart {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecurityStart.class, args);
    }
}
