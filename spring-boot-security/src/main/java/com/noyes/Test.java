package com.noyes;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Test {
    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String pwd = "admin123";
        String encoderPwd1 = encoder.encode(pwd);
        String encoderPwd2 = encoder.encode(pwd);
        String encoderPwd3 = encoder.encode(pwd);
        System.out.println(encoderPwd1);
        System.out.println(encoderPwd2);
        System.out.println(encoderPwd3);
        System.out.println(encoder.matches(pwd,encoderPwd1));
        System.out.println(encoder.matches(pwd,encoderPwd2));
        System.out.println(encoder.matches(pwd,encoderPwd3));

/**
        $2a$10$uzMdaUyZbgCQrCcSkDkL/.E2zlsrNyb8cXojQUUSlNcbX3jqoqV52
        $2a$10$xsPt.XSuBVgH/vLwR9Cfd.sxTYQfEKNKK7z8rcz9JSraP6Bn3Mnq6
        $2a$10$6lCBbKN1WXHLydtO1nmoRuDM67QTeBWZRsqEnVPIuxonASbcf6/fO
        true
        true
        true
*/
    }
}
