package com.noyes.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/salary")
public class SalaryController {

    /**
     * @param
     * @return: java.lang.String
     * @Description: 代表一个查询薪水的后台接口
     * @Author: yan cong
     * @Date: 2021/6/23 10:33
     */
    @RequestMapping("/query")
    public String query() {
        return "salary";
    }
}
