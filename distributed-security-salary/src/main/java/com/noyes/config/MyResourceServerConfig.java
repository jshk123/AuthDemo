package com.noyes.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * @author yan cong
 * @className MyResourceServerConfig
 * @description 资源服务器配置
 * @time 2021/6/23 10:24
 */
@Configuration
public class MyResourceServerConfig extends ResourceServerConfigurerAdapter {

    public static final String RESOURCE_SALARY = "salary";

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//        resources.resourceId(RESOURCE_SALARY)//资源id
//                .tokenServices(tokenService())//使用远程服务验证令牌的服务
//                .stateless(true);//无状态模式
        resources.resourceId(RESOURCE_SALARY)
                //.tokenServices(tokenService())
                //使用jwt令牌
                .tokenStore(tokenStore)
                .stateless(true);
    }

//    private ResourceServerTokenServices tokenService() {
//        RemoteTokenServices services = new RemoteTokenServices();
//        services.setCheckTokenEndpointUrl("http://localhost:53020/uaa/oauth/check_token");
//        services.setClientId("c1");
//        services.setClientSecret("secret");
//        return services;
//
//    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()//校验请求
                .antMatchers("/salary/**")//路径匹配规则
                .access("#oauth2.hasScope('all')")// 需要匹配的scope
                .and().csrf().disable()//
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
