package com.noyes.basicauth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/salary")
public class SalaryController {

    /**
     * @return: java.lang.String
     * @Description: 代表一个查询薪水的后台接口
     * @Author: yan cong
     * @Date: 2021/6/3 10:43
     */
    @GetMapping("/query")
    public String query() {
        return "salary";
    }
}
