package com.noyes.basicauth.controller;

import com.noyes.basicauth.bean.UserBean;
import com.noyes.basicauth.service.AuthService;
import com.noyes.basicauth.util.MyConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/common")
public class LoginController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private AuthService authService;

    @PostMapping("/login")
    public UserBean login(UserBean loginUser, HttpServletRequest request) {
        UserBean user = authService.userLogin(loginUser);
        if (null != user) {
            logger.info("user login succeed");
            request.getSession().setAttribute(MyConstants.FLAG_CURRENTUSER, user);
        } else {
            logger.info("user login failed");
        }
        return user;
    }

    @PostMapping("/getCurrentUser")
    public Object getCurrentUser(HttpSession session) {
        logger.info(session.getId());
        Object attribute = session.getAttribute(MyConstants.FLAG_CURRENTUSER);
        return attribute;
    }

    @PostMapping("/logout")
    public void logout(HttpSession session) {
        session.removeAttribute(MyConstants.FLAG_CURRENTUSER);
    }
}
