package com.noyes.basicauth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mobile")
public class MobileController {
    /**
     * @return: java.lang.String
     * @Description: 代表一个查询手机号的后台接口
     * @Author: yan cong
     * @Date: 2021/6/3 10:41
     */
    @GetMapping("/query")
    public String query() {
        return "mobile";
    }
}
